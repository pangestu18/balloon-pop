﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class AdsManager : MonoBehaviour {

    private BannerView bannerView;
    private InterstitialAd interstitial;
    public DemoAds.Demo unitID;

    void Awake()
    {
        unitID = GameObject.Find("DemoAds").GetComponent<DemoAds.Demo>();
    }

	// Use this for initialization
	void Start () {
#if UNITY_ANDROID

        if (SceneManager.GetActiveScene().name == "MainGame")
        {
            this.RequestBanner();
        }
        else
        {
            RequestInterstitial();
            StartCoroutine(ShowInterstitialAd());
        }
#endif
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void RequestBanner()
    {
        if (this.bannerView != null)
        {
            DestroyBanner();
        }
        
        this.bannerView = new BannerView(unitID.unitAd.banner, AdSize.Banner, AdPosition.Bottom);

        this.bannerView.OnAdLoaded += this.HandleAdBannerLoaded;
        this.bannerView.OnAdFailedToLoad += this.HandleAdBannerFailedToLoad;
        this.bannerView.OnAdOpening += this.HandleAdBannerOpened;
        this.bannerView.OnAdClosed += this.HandleAdBannerClosed;
        this.bannerView.OnAdLeavingApplication += this.HandleAdBannerLeftApplication;

        AdRequest request = new AdRequest.Builder().Build();
        this.bannerView.LoadAd(request);
    }

    public void DestroyBanner()
    {
        this.bannerView.Destroy();
    }

    void RequestInterstitial()
    {
        this.interstitial = new InterstitialAd(unitID.unitAd.interstitial);

        this.interstitial.OnAdLoaded += this.HandleAdInterstitialLoaded;
        this.interstitial.OnAdFailedToLoad += this.HandleAdInterstitialFailedToLoad;
        this.interstitial.OnAdOpening += this.HandleAdInterstitialOpened;
        this.interstitial.OnAdClosed += this.HandleAdInterstitialClosed;
        this.interstitial.OnAdLeavingApplication += this.HandleAdInterstitialLeftApplication;

        AdRequest request = new AdRequest.Builder().Build();
        this.interstitial.LoadAd(request);
    }

    public IEnumerator ShowInterstitialAd()
    {
        yield return new WaitUntil(() => this.interstitial.IsLoaded());

        this.interstitial.Show();
    }

    public void DestroyInterstitialAd()
    {
        this.interstitial.Destroy();
    }

    #region HANDLE BANNER
    public void HandleAdBannerLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleAdBannerFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdBannerOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleAdBannerClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleAdBannerLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeftApplication event received");
    }
    #endregion

    #region HANDLE INTERSTITIAL
    public void HandleAdInterstitialLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleAdInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdInterstitialOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleAdInterstitialClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleAdInterstitialLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeftApplication event received");
    }
    #endregion
}

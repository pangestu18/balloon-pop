﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class TitleScreenScript : MonoBehaviour {

    public Text scoreDisplay;
    AdsManager adsManager;
    //DemoAds.Demo unitID;

    void Awake()
    {
        adsManager = GetComponent<AdsManager>();
        //unitID = GameObject.Find("DemoAds").GetComponent<DemoAds.Demo>();


    }

    public void Start() {
#if UNITY_ANDROID
        MobileAds.Initialize(adsManager.unitID.unitAd.appid);
#endif
        if (PlayerPrefs.HasKey("Points")) {
			scoreDisplay.text = "Score: " + PlayerPrefs.GetInt ("Points").ToString();
		}
	}

	public void LoadGame() {
        adsManager.DestroyInterstitialAd();
        SceneManager.LoadScene("MainGame");
	}

	public void QuitApplication() {
        adsManager.DestroyInterstitialAd();
        Application.Quit();
	}
}

﻿using System;
namespace PengembangSebelah
{
    [System.Serializable]
    public class Unit
    {
        public string appid = "";
        public string banner = "";
        public string interstitial = "";
        public string reward = "";
        public string native = "";

        public Unit(string appid, string banner, string interstitial, string reward, string native)
        {
            this.appid = appid;
            this.banner = banner;
            this.interstitial = interstitial;
            this.reward = reward;
            this.native = native;
        }

    }
}

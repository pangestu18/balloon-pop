﻿using System;
using UnityEngine;
using PengembangSebelah;
namespace DemoAds
{
    public class Demo : MonoBehaviour
    {
        [Header("Untuk Mengetest Hasil Encript dan Decript")]
        public String WhanEncript = "Test";
        public String WhanDecript = "Test";
        [Header("Isian")]
        //Variabel apakah di encrip
        public bool isEncript = false;

        //Link Json jika nilai ("") maka akan melihat link default test
        public String Url = "";
        [Header("Object Unit")]
        public Unit unitAd;

        private static Demo _instance;

        private static Demo Instance { get { return _instance; } }

        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        private void Start()
        {
            Debug.Log("Encript of Whan Encript = (" + Translite.Encrypt(WhanEncript) + ")");
            Debug.Log("Decript of Whan Decript = (" + Translite.Decrypt(WhanDecript) + ")");

            //Membaca Json Online
            Parse parse = new Parse();

            //Eksekusi Link
            StartCoroutine(parse.Run(Url,isEncript, HandleAction));
            
        }

        //Callback action dari unit
        void HandleAction(Unit obj)
        {
            //Output print kode appid
            Debug.Log(obj.appid);
            unitAd = obj;
        }



    }
}

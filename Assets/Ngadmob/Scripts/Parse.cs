﻿using System;
using UnityEngine;
using System.Collections;
namespace PengembangSebelah
{
    using SimpleJSON;

    public class Parse
    {
        string jsonData;
        string url = "https://dl.dropboxusercontent.com/s/pd3w1biuzm4moe6/test.json";
        public IEnumerator Run(String Url,bool IsEncript,Action<Unit> action)
        {

            if (Url != "")
            {
                this.url = Url;
            }

            WWW www = new WWW(url);
            yield return www;

            if (string.IsNullOrEmpty(www.error))
            {
                jsonData = www.text;
            }

            if (jsonData != "")
            {
                JSONNode jsonNode = JSON.Parse(jsonData);
                int isOne = jsonNode["status"];
                if (isOne == 1)
                {
                    if (!IsEncript)
                    {

                        Unit unit = new Unit(jsonNode["appid"], jsonNode["banner"], jsonNode["interstitial"], jsonNode["reward"], jsonNode["native"]);
                        action(unit);
                    }
                    else
                    {
                        String appid = Translite.Decrypt(jsonNode["appid"]);
                        String banner = Translite.Decrypt(jsonNode["banner"]);
                        String inter = Translite.Decrypt(jsonNode["interstitial"]);
                        String reward = Translite.Decrypt(jsonNode["reward"]);
                        String native = Translite.Decrypt(jsonNode["native"]);

                        Unit unit = new Unit(appid, banner, inter, reward, native);
                        action(unit);
                    }
                }
                else
                {
                    Debug.Log("== Ads Status Is Disable ==");
                    Unit unit = new Unit("", "", "", "", "");
                    action(unit);
                }
            }
            else
            {
                Debug.LogError("Link Url Suhu");
            }


        }
    }
}
